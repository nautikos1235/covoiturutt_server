<?php
 
class UserTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('user')->delete();
 
        DB::table('user')->insert(array(
            'id' => 1,
            'username' => 'nico',
            'password' => Hash::make('Nico91'),
            'wrong_tries' => 0,
            'displayed_name' => 'Nicolas',
            'email' => 'nicolas.parquet@utt.fr',
            'phone_number' => '0123456789',
            'created_at' => '2013-12-25 10:31:50'
        ));
        
        DB::table('user')->insert(array(
            'id' => 2,
            'username' => 'ludo',
            'password' => Hash::make('Ludo10'),
            'wrong_tries' => 0,
            'displayed_name' => 'Ludovic',
            'email' => 'ludovic@example.fr',
            'phone_number' => '0123456789',
            'created_at' => '2013-12-25 10:31:50'
        ));
        
        DB::table('user')->insert(array(
            'id' => 3,
            'username' => 'toto',
            'password' => Hash::make('toto'),
            'wrong_tries' => 0,
            'displayed_name' => 'Toto',
            'email' => 'toto@example.fr',
            'phone_number' => '0123456789',
            'created_at' => '2013-12-25 10:31:50'
        ));
    }
 
}