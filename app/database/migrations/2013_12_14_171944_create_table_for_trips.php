<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableForTrips extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('place', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('displayed_name')->unique();
			$table->timestamps();
		});
                
		Schema::create('trip', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->integer('driver_id')->unsigned();
                        $table->integer('departure_place_id')->unsigned();
                        $table->integer('destination_id')->unsigned();
			$table->dateTime('departure_time');
			$table->integer('total_seats');
			$table->string('payment_asked');
			$table->timestamps();
                        $table->foreign('driver_id')->references('id')->on('user');
                        $table->foreign('departure_place_id')->references('id')->on('place');
                        $table->foreign('destination_id')->references('id')->on('place');
		});
                
                Schema::create('trip_participation', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->integer('user_id')->unsigned();
                        $table->integer('trip_id')->unsigned();
			$table->timestamps();
                        $table->foreign('user_id')->references('id')->on('user');
                        $table->foreign('trip_id')->references('id')->on('trip');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trips', function(Blueprint $table)
		{
			//
		});
	}

}