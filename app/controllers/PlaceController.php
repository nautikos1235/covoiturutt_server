<?php

/**
 * Description of PlaceController
 *
 * @author Nico
 */
class PlaceController extends BaseController {
    public function getService() {
        return "PlaceController";
    }
    
    public function getAll() {
        return Place::all()->toArray();
    }
}

?>
