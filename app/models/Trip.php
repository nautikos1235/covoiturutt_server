<?php

/**
 * Description of Trip
 *
 * @author Nico
 */
class Trip extends Eloquent {
    
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'trip';
        
    protected $hidden = array('driver_id');
    
    public function driver()
    {
        return $this->hasOne('User', 'id', 'driver_id');
    }
    
    public function destination()
    {
        return $this->hasOne('Place', 'id', 'destination_id');
    }
    
    public function departurePlace()
    {
        return $this->hasOne('Place', 'id', 'departure_place_id');
    }
    
    public function participants()
    {
        $userId = Auth::user()->id;
        return $this->belongsToMany('User', 'trip_participation')->where('user.id', '!=', $userId)->withPivot('status');;
    }
}

?>
